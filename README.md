# Batmen Tools

Set of useful tools to set up experiements with batmen like workload generation from swf files.  

## Install
You can either directly execute the scripts you want do use, or install the package with 

```
pip install .
```

or 

```
pip install git+https://gitlab.irit.fr/sepia-pub/mael/batmen-tools.git
```

## Dependancies
- pandas
- numpy