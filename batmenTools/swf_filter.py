#!/usr/bin/env python3

"""
SWF parser allowing to filter jobs from an input file according to some criteria.
Inspired from https://gitlab.inria.fr/batsim/batsim/-/blob/master/tools/swf_to_batsim_workload_compute_only.py
"""

import argparse
import re

from batmenTools.swf import SwfField


def filter_workload(input_swf:str, output_swf:str=None,
                    partitions_to_select:list=None,
                    keep_only:str=None,
                    quiet:bool=False):
    """Makes a selection from a SWF trace, optionally outputing it as SWF."""
    element = '([-+]?\d+(?:\.\d+)?)'
    r = re.compile('\s*' + (element + '\s+') * 17 + element + '\s*')

    # Some counters...
    not_selected = {"nb": 0, "coreh": 0}
    selected = {"nb": 0, "coreh": 0}
    not_valid = 0
    not_line_match_format = 0
    users = []

    # Output file
    if output_swf is not None:
        out_swf = open(output_swf, 'w')

    # Let's loop over the lines of the input file
    i = 0
    with open(input_swf, 'r') as swf:
        for line in swf:
            i += 1
            if not quiet and i % 10000 == 0:
                print(f"\r\033[KProcessing swf line {i}", end="")

            res = r.match(line)

            if res:
                # Parsing...
                nb_res = int(
                    float(res.group(SwfField.REQUESTED_NUMBER_OF_PROCESSORS.value)))
                run_time = float(res.group(SwfField.RUN_TIME.value))
                submit_time = max(0, float(res.group(SwfField.SUBMIT_TIME.value)))
                walltime = float(res.group(SwfField.REQUESTED_TIME.value))
                user_id = str(res.group(SwfField.USER_ID.value))
                partition_id = int(res.group(SwfField.PARTITION_ID.value))

                # Select jobs to keep
                is_valid_job = (nb_res > 0 and run_time >= 0 and submit_time >= 0)
                select_partition = ((partitions_to_select is None) or
                                    (partition_id in partitions_to_select))
                use_job = select_partition and (
                    (keep_only is None) or eval(keep_only))

                # Increment counters
                if not is_valid_job:
                    not_valid += 1
                elif not use_job:
                    not_selected["nb"] += 1
                    not_selected["coreh"] += run_time * nb_res
                else:
                    selected["nb"] += 1
                    selected["coreh"] += run_time * nb_res
                    if user_id not in users:
                        users.append(user_id)

                    # Output in the swf
                    if output_swf is not None:
                        out_swf.write(line)

            else:
                not_line_match_format += 1

    if not quiet:
        print("\n-------------------\nEnd parsing.")
        print(
            f"Total {selected['nb']} jobs and {len(users)} users have been created.")
        print(f"Total number of core-hours: {selected['coreh'] / 3600:.0f}")
        print(f"{not_selected['nb']} valid jobs were not selected (keep_only) "
              f"for {not_selected['coreh'] / 3600:.0f} core-hour")
        print("Jobs not selected: {:.1f}% in number, {:.1f}% in core-hour"
              .format(not_selected["nb"] / (not_selected["nb"]+selected["nb"]) * 100,
                      not_selected["coreh"] / (selected["coreh"]+not_selected["coreh"]) * 100))
        print(f"{not_line_match_format} out of {i} lines in the file did not "
              "match the swf format")
        print(f"{not_valid} jobs were not valid")


def main():
    """
    Program entry point.

    Parses the input arguments then calls filter_workload.
    """
    parser = argparse.ArgumentParser(
        description="Filter jobs from the SWF input according to some criteria "
        "and display the proportion of jobs kept, optionnally outputing the "
        "selection in a new SWF file.")
    parser.add_argument('input_swf', type=str,
                        help='The input SWF file')
    parser.add_argument('-o', '--output_swf',
                        type=str, default=None,
                        help='The optional output SWF file.')
    parser.add_argument('-sp', '--partitions_to_select',
                        type=int, nargs='+', default=None,
                        help="List of partitions to only consider in the input "
                        "trace. The jobs running in the other partitions will "
                        "be discarded.")

    parser.add_argument('--keep_only',
                        type=str,
                        default=None,
                        help="If set, this parameter is evaluated to choose "
                        "which jobs should be kept")

    parser.add_argument("-q", "--quiet", action="store_true")

    args = parser.parse_args()

    filter_workload(input_swf=args.input_swf,
                    output_swf=args.output_swf,
                    partitions_to_select=args.partitions_to_select,
                    keep_only=args.keep_only,
                    quiet=args.quiet)


if __name__ == "__main__":
    main()
