from batmenTools.swf2batsim_split_by_user import generate_workload
from batmenTools.swf_filter import filter_workload

import os.path, sys, subprocess

input_swf=os.path.abspath("test/input/mock.swf")
output_dir = os.path.abspath("test-out")
if not os.path.exists(output_dir):
        os.makedirs(output_dir) 

def test_split_by_user():
    """Very simple test if split_by_user works without error"""
    generate_workload(input_swf, output_folder=output_dir)

def test_CLI():
    """Very simple test of the CLI of our different tools"""

    subprocess.run([sys.executable, "batmenTools/swf2batsim_split_by_user.py",
                    input_swf, output_dir], check=True)
    subprocess.run([sys.executable, "batmenTools/swf_filter.py",
                    input_swf, "-o", f"{output_dir}/bla.swf"], check=True)
